/***
 * 
 * 
 *  Helcim Transaction Options
 * 
 */
export type HelcimTransactionOptions = {

    // Card Information - Required
    cardNumber: string
    cardExpiryMonth: string
    cardExpiryYear: string
    cardCVV: string
    // cardToken: string

    // AVS Information - Required
    cardHolderName: string,
    cardHolderAddress: string
    cardHolderPostalCode: string

    // Amount Information
    amount: string | "0.00"
    currency: "CAD" | "USD"
    currencyHash?: string | null
    amountHash?: string | null
    orderNumber?: string | null
    customerCode?: string | null
    amountShipping?: string | null
    amountTax?: string | null
    amountDiscount?: string | null
    comments?: string | null

    // Optional Billing Information
    billingContactName?: string | null
    billingBusinessName?: string | null
    billingStreet1?: string | null
    billingStreet2?: string | null
    billingCity?: string | null
    billingProvince?: string | null
    billingPostalCode?: string | null
    billingCountry?: string | null
    billingPhone?: string | null
    billingFax?: string | null
    billingEmail?: string | null

    // Optional Shipping Information
    shippingContactName?: string | null
    shippingBusinessName?: string | null
    shippingStreet1?: string | null
    shippingStreet2?: string | null
    shippingCity?: string | null
    shippingProvince?: string | null
    shippingPostalCode?: string | null
    shippingCountry?: string | null
    shippingPhone?: string | null
    shippingFax?: string | null
    shippingEmail?: string | null

    captcha?: string | null
}