/***
 * 
 * 
 *  Helcim Response Type
 * 
 */
export type HelcimResponse = {
    status: "success"
    data: {
        amount: string | null
        approvalCode: string | null,
        avsResponse: string | null,
        cardExpiry: string | null
        cardHolderName: string | null
        cardNumber: string | null
        cardToken: string | null
        cardType: string | null
        currency: "CAD" | "USD" | null
        customerCode: string | null
        cvvResponse: string | null
        date: string | null
        includeXML: string | null
        noticeMessage: string | null
        orderNumber: string | null
        response: string | null
        responseMessage: string | null
        time: string | null
        transactionId: string | null
        type: string | null
        xmlHash: string | null
    }
};