/***
 * 
 * 
 *  Helcim Error Type
 * 
 */

import { HelcimErrorCode } from "./HelcimErrorCode.type"

export type HelcimError = {
    status: "error"
    errorCode: HelcimErrorCode[]
}