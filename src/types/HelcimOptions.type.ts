/***
 * 
 * 
 *  Helcim Options Type
 * 
 */

export type HelcimOptions = {
    token: string,
    test?: boolean
}