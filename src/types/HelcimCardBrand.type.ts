/***
 * 
 * 
 *  Helcim Card Brand Type
 * 
 */
export type HelcimCardBrand =  "american-express"
| "diners-club"
| "discover"
| "elo"
| "hiper"
| "hipercard"
| "jcb"
| "maestro"
| "mastercard"
| "mir"
| "unionpay"
| "visa"
| "unknown"