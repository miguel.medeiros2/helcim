/***
 * 
 * 
 *  Helcim Typescript SDK
 * 
 *  Copyright (C) 2024 Miguel Medeiros. All Rights Reserved.
 */

import type { HelcimCardBrand } from "./types/HelcimCardBrand.type";
import type { HelcimError } from "./types/HelcimError.type";
import type { HelcimOptions } from "./types/HelcimOptions.type";
import type { HelcimResponse } from "./types/HelcimResponse.type";
import type { HelcimTransactionOptions } from "./types/HelcimTransactionOptions.type";
import * as valid from "card-validator"
import * as creditCardType from "credit-card-type"
import axios from "axios"
import * as qs from "qs"
import {convertXML} from "simple-xml-to-json"

export class HelcimClient {

    private token: string;
    private test: boolean;
    private xmlResponseData: string;

    constructor(options: HelcimOptions) {
        this.token = options.token;
        this.test = options.test || false;
    }

    /** 
     * Process the Transaction
     */
    public async processTransaction(parameters: HelcimTransactionOptions): Promise<HelcimResponse | HelcimError> {

        // Manipulate the card expiry
        let cardExpiry = ""
        const cardExpiryMonth = parameters.cardExpiryMonth
        const cardExpiryYear = parameters.cardExpiryYear

        // Validate SSL
        if (!HelcimClient.validateBrowserIsSSL()) return {status: "error", errorCode: ["BROWSER_SSL_CERT_MISSING"]}


        // Check if the card number is valid
        if (!HelcimClient.validateCardNumber(parameters.cardNumber)) return {status: "error", errorCode: ["INVALID_CARDNUMBER"]} as HelcimError


        // Check if the card expiry is valid
        if (HelcimClient.validateCardExpiry(`${cardExpiryMonth}/${cardExpiryYear}`) && cardExpiryMonth.length === 2) {
            cardExpiry = `${cardExpiryMonth}${cardExpiryYear.slice(-2)}`
        } else {

            return {status: "error", errorCode: ["INVALID_EXPIRY"]} as HelcimError

        }

        // Check if the CVV is valid
        if (!HelcimClient.validateCardCvv(parameters.cardCVV)) return {status: "error", errorCode: ["INVALID_CVV"]} as HelcimError

        

        let queryObject = {
            token: this.token,
            test: this.test === true ? "1" : "0",
            cardNumber: parameters.cardNumber,
            cardExpiry: cardExpiry,
            cardCVV: parameters.cardCVV,
            cardHolderName: parameters.cardHolderName,
            cardHolderAddress: parameters.cardHolderAddress,
            cardHolderPostalCode: parameters.cardHolderPostalCode.replace(" ", "").trim(),
        }

        if (parameters.amount) queryObject["amount"] = parameters.amount
        if (parameters.currency) queryObject["currency"] = parameters.currency
        if (parameters.currencyHash) queryObject["currencyHash"] = parameters.currencyHash
        if (parameters.amountHash) queryObject["amountHash"] = parameters.amountHash
        if (parameters.orderNumber) queryObject["orderNumber"] = parameters.orderNumber
        if (parameters.customerCode) queryObject["customerCode"] = parameters.customerCode

        if (parameters.amountShipping) queryObject["amountShipping"] = parameters.amountShipping
        if (parameters.amountTax) queryObject["amountTax"] = parameters.amountTax
        if (parameters.amountDiscount) queryObject["amountDiscount"] = parameters.amountDiscount
        if (parameters.comments) queryObject["comments"] = parameters.comments

        // Billing Data
        if (parameters.billingContactName) queryObject["billing_contactName"] = parameters.billingContactName
        if (parameters.billingBusinessName) queryObject["billing_businessName"] = parameters.billingBusinessName
        if (parameters.billingStreet1) queryObject["billing_street1"] = parameters.billingStreet1
        if (parameters.billingStreet2) queryObject["billing_street2"] = parameters.billingStreet2
        if (parameters.billingCity) queryObject["billing_city"] = parameters.billingCity
        if (parameters.billingProvince) queryObject["billing_province"] = parameters.billingProvince
        if (parameters.billingPostalCode) queryObject["billing_postalCode"] = parameters.billingPostalCode.replace(" ", "").trim()
        if (parameters.billingCountry) queryObject["billing_country"] = parameters.billingCountry
        if (parameters.billingPhone) queryObject["billing_phone"] = parameters.billingPhone
        if (parameters.billingFax) queryObject["billing_fax"] = parameters.billingFax
        if (parameters.billingEmail) queryObject["billing_email"] = parameters.billingEmail

        // Shipping Data
        if (parameters.shippingContactName) queryObject["shipping_contactName"] = parameters.shippingContactName
        if (parameters.shippingBusinessName) queryObject["shipping_businessName"] = parameters.shippingBusinessName
        if (parameters.shippingStreet1) queryObject["shipping_street1"] = parameters.shippingStreet1
        if (parameters.shippingStreet2) queryObject["shipping_street2"] = parameters.shippingStreet2
        if (parameters.shippingCity) queryObject["shipping_city"] = parameters.shippingCity
        if (parameters.shippingProvince) queryObject["shipping_province"] = parameters.shippingProvince
        if (parameters.shippingPostalCode) queryObject["shipping_postalCode"] = parameters.shippingPostalCode
        if (parameters.shippingCountry) queryObject["shipping_country"] = parameters.shippingCountry
        if (parameters.shippingPhone) queryObject["shipping_phone"] = parameters.shippingPhone
        if (parameters.shippingFax) queryObject["shipping_fax"] = parameters.shippingFax
        if (parameters.shippingEmail) queryObject["shipping_email"] = parameters.shippingEmail

        // Captcha
        if (parameters.captcha) queryObject["g-recaptcha-response"] = parameters.captcha

        // Create the URL Params
        const urlParams = qs.stringify(queryObject)

        try {

            const request = await axios.post("https://secure.myhelcim.com/js", urlParams, { headers: { "Content-Type": "application/x-www-form-urlencoded"}})

            this.xmlResponseData = request.data

        } catch (err) {


            return { status: "error", errorCode: ["GENERAL_ERROR"]}

        }



        // Parse the XML
        const result = await convertXML(this.xmlResponseData)

        const data = result.message.children.map((child: any) => { 

            return {
                [Object.keys(child)[0]]: child[Object.keys(child)[0]].content ?? null
            }

        })

        const dataObject = Object.assign({}, ...data)

        return { status: "success", data: dataObject}

    }

    /***
     * 
     * Validates a Card Number and returns a boolean of whether it is valid or not
     * 
     */
    static validateCardNumber(cardNumber: string): boolean {

        return valid.number(cardNumber).isValid

    }

    /** Get a card brand based on the credit card number (even from a partial number) */
    static getCardBrand(cardNumber: string): HelcimCardBrand  {

        // Types
        const types = [
            "american-express",
            "diners-club",
            "discover",
            "elo",
            "hiper",
            "hipercard",
            "jcb",
            "maestro",
            "mastercard",
            "mir",
            "unionpay",
            "visa"
        ]

        return types.indexOf(creditCardType(cardNumber)[0].type) > -1 ? creditCardType(cardNumber)[0].type as HelcimCardBrand : "unknown" as HelcimCardBrand

    }

    /***
     * Validates a Card Expiry and returns a boolean of whether it is valid or not
     */
    static validateCardExpiry(cardExpiry: string): boolean {

        return valid.expirationDate(cardExpiry).isValid

    }

    /***
     * Validates a Card CVV and returns a boolean of whether it is valid or not
     */
    static validateCardCvv(cardCvv: string): boolean {

        return valid.cvv(cardCvv).isValid

    }

    /***
     * Validate SSL
     */
    static validateBrowserIsSSL(): boolean {

        if (document.location.protocol === "https:") return true

        return false
    }


}