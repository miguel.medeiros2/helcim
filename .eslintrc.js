module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
      project: 'tsconfig.json',
      tsconfigRootDir: __dirname,
      sourceType: 'module',
    },
    plugins: ['@typescript-eslint/eslint-plugin'],
    extends: [
      'plugin:@typescript-eslint/recommended'
    ],
    root: true,
    env: {
      node: true,
      jest: true,
    },
    ignorePatterns: ['.eslintrc.js'],
    rules: {
      '@typescript-eslint/interface-name-prefix': 'off',
      '@typescript-eslint/explicit-function-return-type': 'off',
      '@typescript-eslint/explicit-module-boundary-types': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      "no-trailing-spaces": "error",
      "no-multiple-empty-lines": "error",
      "indent": ["error", 4, { "SwitchCase": 1 }],
      "@typescript-eslint/indent": ["error", 4, { "SwitchCase": 1 }],
      "linebreak-style": ["error", "unix"],
      "quotes": ["error", "double"],
      "prefer-spread": "off",
      "comma-dangle": [
        "error",
        {
          "arrays": "only-multiline",
          "objects": "only-multiline",
          "imports": "never",
          "exports": "never",
          "functions": "never"
        }
      ],
      "@typescript-eslint/no-unused-vars": [
        "off", // or "error"
        {
          "argsIgnorePattern": "^_",
          "varsIgnorePattern": "^_",
          "caughtErrorsIgnorePattern": "^_"
        }
      ],
      "semi": ["error", "never"],
      "no-unused-vars": "off",
      "object-curly-spacing": ["error", "always"]
    },
  };
  