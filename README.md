# Helcim Client Typescript
This library is a community-built library intended to replace Helcim.js for Verify Transactions. Instead of using form fields like Helcim.js, it provides a Javascript (with Typescript types) API that you can use in the frontend of your application. 

This is especially useful if you want a custom field design and are using a Frontend framework like VueJS, React, or Angular, where Helcim.js' method of manipulating the DOM directly is a bad idea.

**It is strongly recommended that you use this library for Verify Transactions ONLY**
It is much more secure to process purchase or capture transactions on your backend, and only
transfer the customer information / tokenized Card data to your backend over an SSL connection.

## How it works
This library uses Axios (an AJAX library), QS (Query String for URL-encoded data), and an XML Parser to send information to and from Helcim, and is very similar to Helcim's Helcim.js client except the data is passed in as a Javascript Object and is returned as a Javascript object. Here is an example implementation:


First, install the client in your VueJS/React/Angular Project:
```bash
$ npm install @tapanytime/helcim
```

```typescript
// Import the Helcim Client in your frontend code in vue or react
import * as SDK from "@tapanytime/helcim"

// Create a Helcim Client Instance, passing in the Helcim.js token that you created in the Helcim Portal
const client = new SDK.HelcimClient({ token: "YOUR_HELCIMJS_TOKEN_HERE" })

// Run this in an asynchronous function
const result = await client.processTransaction({
    cardNumber: "5413330089099130", // This is a sample card number
    cardExpiryYear: "2025", // Doesn't matter if you put just the last two digits of the year or the full year, but it will only accept years in the next 19 years
    cardExpiryMonth: "01", // Doesn't matter if its a single digit month or two digit month
    cardCVV: "100", // Can be 3 or 4 numbers
    
    amount: "0.00",
    cardHolderName: "John Doe",
    cardHolderAddress: "123 Streetname St.",
    cardHolderPostalCode: "A1A 1A1", // Spaces in between will be automatically removed
})


console.log(result)
// { status: "success", data: { ... }}

// or

// { status: "error", errorCodes: [...] }

```

There are also a few functions implemented to help with your UI:

```typescript
import * as SDK from "@tapanytime/helcim"

// This supports partial card numbers as well and it will give a best guess
const result = SDK.HelcimClient.getCardBrand("5413330089099130")

console.log(result)

// mastercard

// or unknown (if not a matching card type)
// See src/types/HelcimCardBrand.type.ts for all the possible card types
// This doesn't mean that helcim supports these cards

```

## Building
Building this project is quite simple. Ensure you have webpack installed and then in the root directory of the project:

```bash
$ npm install
```

and 

```bash
$ npm run build
```

This will output the code into a newly created `dist` folder, under bundle.js. That will allow you to include it in your web project with a script tag if you are not using a Vue/Angular/React project.

## Limitations
This library does not currently implement Bank Transactions or Line Items. It only implements Credit Card Transactions and is intended only for Verify Transactions. If this feature is requested I can implement it, as it's very easy to do.

It also doesn't parse errors that may occur on the Helcim backend yet.

## Important
If you try to test this on localhost without an SSL certificate, it will not work. This is a limitation of the Helcim.js API, **not of this library**. It throws an internal server error otherwise. Ensure that the endpoint you are running this off of is using HTTPS. One workaround to this is to use something like `ngrok` to expose your local server instance (ngrok by default converts your http site to https) to the internet and then connecting to it.