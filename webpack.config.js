const path = require('path');
module.exports = [{
    target: "web",
   entry: "./src/index.ts",
   output: {
       filename: "bundle.js",
       path: path.resolve(__dirname, 'dist'),
       library: {
        name: 'HelcimClient',
        type:  "umd"
      },
   },
   resolve: {
       extensions: [".webpack.js", ".web.js", ".ts", ".js"]
   },
   module: {
       rules: [{ test: /\.ts$/, loader: "ts-loader" }]
   }
}]
